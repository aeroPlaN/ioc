﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Castle.Windsor;

namespace CCD_IoC.MessageBus
{
    public class IocMessageBus : IMessageBus
    {
        private readonly IWindsorContainer container;

        public IocMessageBus(IWindsorContainer container)
        {
            this.container = container;
        }

        public void Publish<TMessage>(TMessage message) where TMessage : class, IMessage
        {
            var eventHandlers = container.ResolveAll<IMessageHandler<TMessage>>();
            foreach (var eventHandler in eventHandlers)
            {
                eventHandler.Handle(message);
            }
        }
    }
}