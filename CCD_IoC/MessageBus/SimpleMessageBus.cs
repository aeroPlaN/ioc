﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCD_IoC.MessageBus
{
    internal class SimpleMessageBus : IMessageBus
    {
        Dictionary<Type, IList> _subscribers = new Dictionary<Type, IList>();

        public SimpleMessageBus()
        {
        }

        public void Subscribe<TMessage>(IMessageHandler<TMessage> handler) where TMessage : class, IMessage
        {
            _subscribers[typeof(TMessage)] = new GenericList<IMessageHandler<TMessage>>();
            _subscribers[typeof(TMessage)].Add(handler);
        }

        public void Publish<TMessage>(TMessage message) where TMessage : class, IMessage
        {
            if (_subscribers.ContainsKey(typeof(TMessage)))
            {
                var handlers = _subscribers[typeof(TMessage)];
                foreach (IMessageHandler<TMessage> handler in handlers)
                {
                    handler.Handle(message);
                }
            }
        }
    }
}