﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCD_IoC.MessageBus
{
    public interface IMessageHandler<T> where T : IMessage
    {
        void Handle(T message);
    }
}