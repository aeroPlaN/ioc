﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCD_IoC.MessageBus
{
    internal class GenericList<T> : List<T>, IList
    {
        public void Add(object item)
        {
            base.Add((T)item);
        }
    }
}