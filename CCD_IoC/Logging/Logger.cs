﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCD_IoC.Logging
{
    public class Logger : ILogger
    {
        public Logger() { }

        public void Log(string logEntry)
        {
            Console.WriteLine(logEntry);
        }
    }
}