﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCD_IoC.Logging
{
    public interface ILogger
    {
        void Log(string logEntry);
    }
}