﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CCD_IoC.Logging;

namespace CCD_IoC.Service
{
    public class AwesomeService : IService
    {
        private ILogger _logger;

        public AwesomeService(ILogger logger)
        {
            _logger = logger;
        }

        public void Execute()
        {
            _logger.Log("AwesomeService: I'm so awesome. I have nothing to do! :)");
        }
    }
}