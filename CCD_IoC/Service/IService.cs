﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CCD_IoC.Service
{
    public interface IService
    {
        void Execute();
    }
}