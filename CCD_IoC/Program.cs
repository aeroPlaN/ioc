﻿using System;
using System.Collections.Generic;
using System.Linq;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using CCD_IoC.Logging;
using CCD_IoC.MessageBus;
using CCD_IoC.MessageHandler;
using CCD_IoC.Messages;
using CCD_IoC.Service;

namespace CCD_IoC
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Init();
            InitIoC();
            Console.ReadKey();
        }

        private static void Init()
        {
            var logger = new Logger();
            var messageBus = new SimpleMessageBus();
            var pingHandler = new PingHandler(logger, messageBus);
            var awesomeService = new AwesomeService(logger);
            var pongHandler = new PongHandler(logger, messageBus, awesomeService);

            messageBus.Subscribe<Ping>(pingHandler);
            messageBus.Subscribe<Pong>(pongHandler);

            messageBus.Publish<Ping>(new Ping());
        }

        private static void InitIoC()
        {
            IWindsorContainer container = new WindsorContainer();
            container.Register(Component.For<IWindsorContainer>().Instance(container));

            //container.Register(Component.For<ILogger>().ImplementedBy<Logger>());
            //container.Register(Component.For<IMessageBus>().ImplementedBy<IocMessageBus>());
            //container.Register(Component.For<IMessageHandler<Ping>>().ImplementedBy<PingHandler>());
            //container.Register(Component.For<IMessageHandler<Pong>>().ImplementedBy<PongHandler>());
            //container.Register(Component.For<IService>().ImplementedBy<AwesomeService>());

            container.Register(Classes.FromThisAssembly().InNamespace("CCD_IoC.Logging").WithServiceAllInterfaces());
            container.Register(Classes.FromThisAssembly().InNamespace("CCD_IoC.MessageBus").WithServiceAllInterfaces());
            container.Register(Classes.FromThisAssembly().InNamespace("CCD_IoC.MessageHandler").WithServiceAllInterfaces());
            container.Register(Classes.FromThisAssembly().InNamespace("CCD_IoC.Service").WithServiceAllInterfaces());

            var messageBus = container.Resolve<IMessageBus>();
            messageBus.Publish<Ping>(new Ping());
        }
    }
}