﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CCD_IoC.Logging;
using CCD_IoC.MessageBus;
using CCD_IoC.Messages;
using CCD_IoC.Service;

namespace CCD_IoC.MessageHandler
{
    public class PongHandler : IMessageHandler<Pong>
    {
        private ILogger _logger;
        private IMessageBus _messageBus;
        private IService _service;

        public PongHandler(ILogger logger, IMessageBus messageBus, IService service)
        {
            _logger = logger;
            _messageBus = messageBus;
            _service = service;
        }

        public void Handle(Pong message)
        {
            _logger.Log("Pong received!\nTrigger the service:");
            _service.Execute();
        }
    }
}