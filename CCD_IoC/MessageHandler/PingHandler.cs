﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CCD_IoC.Logging;
using CCD_IoC.MessageBus;
using CCD_IoC.Messages;

namespace CCD_IoC.MessageHandler
{
    public class PingHandler : IMessageHandler<Ping>
    {
        private ILogger _logger;
        private IMessageBus _messageBus;

        public PingHandler(ILogger logger, IMessageBus messageBus)
        {
            _logger = logger;
            _messageBus = messageBus;
        }

        public void Handle(Ping message)
        {
            _logger.Log("Ping");
            _messageBus.Publish<Pong>(new Pong());
        }
    }
}